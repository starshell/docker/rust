#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
A script to build and deploy this repositories docker containers.
"""
from __future__ import division, print_function, absolute_import

import os
import subprocess
import argparse
import sys
import logging
import datetime
import time
from contextlib import contextmanager
#import yaml
import glob

__author__ = "Derek Goddeau"
__copyright__ = "Derek Goddeau"
__license__ = "MPL 2.0"

__version__ = "0.1.0"

_logger = logging.getLogger(__name__)

CONTAINER_TEST_IMAGE_BASE_URI = "registry.gitlab.com/starshell/docker/rust/test"
CONTAINER_RELEASE_IMAGE_BASE_URI = "registry.gitlab.com/starshell/docker/rust"

def run():
    """Entry point for console_scripts
    """
    main(sys.argv[1:])


def main(args):
    """Main entry point allowing external calls

    Args:
      args ([str]): command line parameter list
    """
    args = parse_args(args)
    setup_logging(args.loglevel, args.timestamp)

    _logger.info("Searching for dockerfiles")

    builds = []
    for dockerfile in glob.iglob('**/Dockerfile', recursive=True):
        _logger.debug("Found dockerfile: %s",  dockerfile)
        build = DockerBuild(dockerfile)
        _logger.debug("Added build directory: %s",  build.directory)
        _logger.debug("Added image name: %s",  build.image_name)
        builds.append(build)

    for build_context in builds:
        build_container(build_context)


class DockerBuild:
    """Get information needed to build and name the image.

    Images are tagged with the name of the folder they are in.

    """
    def __init__(self, path):
        self.directory = '/'.join(path.split('/')[:-1])
        self.image_name = path.split('/')[-2]

def build_container(build_context): 
    """Build a stage.

    """
    test_image = CONTAINER_TEST_IMAGE_BASE_URI + ":" + build_context.image_name
    release_image = CONTAINER_RELEASE_IMAGE_BASE_URI + ":" + build_context.image_name 
    _logger.info("Test image URI set to: %s", test_image)
    _logger.info("Release image URI set to: %s", release_image)
    with cd(build_context.directory):
        _logger.info("Building %s image", build_context.image_name)
        build_test_image(test_image)
        _logger.info("Deploying %s image", build_context.image_name)
        build_release_image(test_image, release_image)

def build_test_image(image_name):
    """Build and push a test image.

    """
    subprocess.run(["docker", "build", "-t", image_name, "."], check=True)
    subprocess.run(["docker", "push", image_name], check=True)

def build_release_image(test_image, release_image):
    """Build and push a release image.

    """
    subprocess.run(["docker", "pull", test_image], check=True)
    subprocess.run(["docker", "tag", test_image, release_image], check=True)
    subprocess.run(["docker", "push", release_image], check=True)

@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)

def parse_args(args):
    """Parse command line parameters

    Args:
      args ([str]): command line parameters as list of strings

    Returns:
      :obj:`argparse.Namespace`: command line parameters namespace
    """
    parser = argparse.ArgumentParser(
        description="Build docker containers")
    parser.add_argument(
        '--version',
        action='version',
        version='cat_detector {ver}'.format(ver=__version__))
    parser.add_argument(
        '-v',
        '--verbose',
        dest="loglevel",
        help="set loglevel to INFO",
        action='store_const',
        const=logging.INFO)
    parser.add_argument(
        '-vv',
        '--very-verbose',
        dest="loglevel",
        help="set loglevel to DEBUG",
        action='store_const',
        const=logging.DEBUG)
    parser.add_argument(
        '-t',
        '--timestamp',
        dest="timestamp",
        help="Add timestamps when logging",
        action='store_const',
        const=logging.INFO)
    return parser.parse_args(args)


def setup_logging(loglevel, timestamp):
    """Setup basic logging

    Args:
      loglevel (int): minimum loglevel for emitting messages
    """
    logformat = "[%(levelname)s][%(name)s] %(message)s"
    if timestamp:
        logformat = "[%(asctime)s]" + logformat
    logging.basicConfig(level=loglevel, stream=sys.stdout,
                        format=logformat, datefmt="%Y-%m-%d %H:%M:%S")


if __name__ == "__main__":
    run()
