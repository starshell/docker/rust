# rustfmt

To test if code is formatted correctly:

```bash
cargo fmt --all -- --write-mode=diff
```
